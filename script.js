var grid;
var score = 0;
var pairs = 0;
var alreadyOpen = null;
var timeLock = false;

$(document).ready(function() {

  var x = 4, y = 4;
  generateGrid(x, y);

  $('.card').on( 'click', function() {
    resolveCard( $(this) );
    if (pairs == (x * y) / 2){
    if (confirm("You Won! Score: " + score + "\nDo you want to try again?")) {
      location.reload();
    }
  }
  });

});

async function resolveCard( card ) {

  if ( timeLock ) {
    return;
  }

  var scoreContainer = $('#score');

  timeLock = true;

  let x = card.data('x');
  let y = card.data('y');

  let element = grid[x][y];

  if ( element.state == 1 ) {
    timeLock = false;
    return;
  }

  if ( alreadyOpen == null ) {

    flipCard( card, element, 1 );
    alreadyOpen = card;

  } else {

    flipCard( card, element, 1 );
    let oldCard = alreadyOpen;

    let oldX = oldCard.data('x');
    let oldY = oldCard.data('y');

    let oldElement = grid[ oldX ][ oldY ];

    if ( oldElement.value != element.value ) {

      await sleep(1000);

      flipCard( card, element, 0 );
      flipCard( oldCard, oldElement, 0 );

    }
    else {
      card.removeClass('card-selection');
      card.addClass('card-open');
      oldCard.removeClass('card-selection');
      oldCard.addClass('card-open');
      pairs++;
    }

    alreadyOpen = null;
    score++;
    scoreContainer.text(score);

  }

  timeLock = false;

}

function flipCard( card, element, state ) {

  if ( state == 0 && element.state != 0 ) {
    
    card.removeClass('card-selection');
    card.addClass('card-closed');

    element.state = 0;

  } else if ( state == 1 && element.state != 1 ) {

    card.removeClass('card-closed');
    card.addClass('card-selection');

    element.state = 1;

  }

}

function generateGrid(x, y) {

  var values = [...Array( (x * y ) / 2 ).keys()];

  values = values.concat(values);
  values = shuffle(values);

  grid = [];

  for ( let i = 0; i < x; i++ ) {

    let tmp = [];

    for ( let j = 0; j < y; j++ ) {
      tmp.push( { value: values.pop(), state: 0 } );
    }

    grid.push( tmp );

  }

  var container = $('#grid');

  for ( let i = 0; i < x; i++ ) {
    for ( let j = 0; j < y; j++ ) {
      container.append( '<div class="card card-closed" data-x="'+i+'" data-y="'+j+'" >' + grid[i][j].value + '</div>' );
    }

    container.append( '<br>' );
  }

}

function shuffle(array) {
  var currentIndex = array.length, temporaryValue, randomIndex;

  // While there remain elements to shuffle...
  while (0 !== currentIndex) {

    // Pick a remaining element...
    randomIndex = Math.floor(Math.random() * currentIndex);
    currentIndex -= 1;

    // And swap it with the current element.
    temporaryValue = array[currentIndex];
    array[currentIndex] = array[randomIndex];
    array[randomIndex] = temporaryValue;
  }

  return array;
}

function sleep(ms) {
  return new Promise(resolve => setTimeout(resolve, ms));
}